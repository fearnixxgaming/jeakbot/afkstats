package de.fearnixx.jeak.afkstats;

import de.fearnixx.afkutils.api.state.IAFKEvent;
import de.fearnixx.afkutils.api.state.IClientEnterAFK;
import de.fearnixx.afkutils.api.state.IClientLeaveAFK;
import de.fearnixx.afkutils.api.state.IClientWarnAFK;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.JeakBotPlugin;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.t3.statistics.EventType;
import de.fearnixx.t3.statistics.LogEntry;
import org.hibernate.QueryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JeakBotPlugin(id = "afkstats")
public class AFKStatsBridge {

    private static final Logger logger = LoggerFactory.getLogger(AFKStatsBridge.class);

    @Inject
    @PersistenceUnit(name = "statistics")
    private EntityManager entityManager;

    private EventType enterAFKType;
    private EventType leaveAFKType;
    private EventType warnAFKType;

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        List<EventType> enterAfkTypes = entityManager.createQuery("select t from EventType t WHERE t.ts3Event = 'enterAfk'", EventType.class).getResultList();
        List<EventType> leaveAfkTypes = entityManager.createQuery("select t from EventType t WHERE t.ts3Event = 'leaveAfk'", EventType.class).getResultList();
        List<EventType> warnAfkTypes = entityManager.createQuery("select t from EventType t WHERE t.ts3Event = 'warnAfk'", EventType.class).getResultList();

        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (enterAfkTypes.isEmpty()) {
                enterAFKType = new EventType();
                enterAFKType.setTs3Event("enterAfk");
                enterAFKType.setDescription("A client has become AFK");
                entityManager.persist(enterAFKType);

            } else if (enterAfkTypes.size() > 1) {
                logger.error("Multiple event types registered for enterAFK event! {}", enterAfkTypes);
                event.cancel();
                return;
            } else {
                enterAFKType = enterAfkTypes.get(0);
            }

            if (leaveAfkTypes.isEmpty()) {
                leaveAFKType = new EventType();
                leaveAFKType.setTs3Event("leaveAfk");
                leaveAFKType.setDescription("A client is no longer AFK");
                entityManager.persist(leaveAFKType);

            } else if (leaveAfkTypes.size() > 1) {
                logger.error("Multiple event types registered for leaveAFK event! {}", leaveAfkTypes);
                event.cancel();
                return;
            } else {
                leaveAFKType = leaveAfkTypes.get(0);
            }

            if (warnAfkTypes.isEmpty()) {
                warnAFKType = new EventType();
                warnAFKType.setTs3Event("warnAfk");
                warnAFKType.setDescription("A client will become inactive.");
                entityManager.persist(warnAFKType);

            } else if (warnAfkTypes.size() > 1) {
                logger.error("Multiple event types registered for warnAFK event! {}", warnAfkTypes);
                event.cancel();
                return;

            } else {
                warnAFKType = warnAfkTypes.get(0);
            }

            entityManager.detach(enterAFKType);
            entityManager.detach(leaveAFKType);
            entityManager.detach(warnAFKType);
            transaction.commit();
        } catch (Exception e) {
            logger.error("Failed to get/create event types!", e);
            if (transaction.isActive()) {
                transaction.rollback();
            }
            event.cancel();
        }
    }

    @Listener
    public void onClientEnterAFK(IClientEnterAFK event) {
        persistAfkEvent(event.getReason().toString(), enterAFKType, event);
    }

    @Listener
    public void onClientLeaveAFK(IClientLeaveAFK event) {
        persistAfkEvent(event.getReason().toString(), leaveAFKType, event);
    }

    @Listener
    public void onClientWarnAFK(IClientWarnAFK event) {
        Map<String, String> data = new HashMap<>();
        data.put("idleRemain", Integer.toString(event.getIdleRemaining()));

        LogEntry entry = new LogEntry();
        entry.setEventType(warnAFKType);
        entry.setUserUID(event.getTarget().getClientUniqueID());
        entry.setRawData(serializeMap(data));

        persist(entry);
    }

    private void persistAfkEvent(String reasonStr, EventType type, IAFKEvent event) {
        Map<String, String> data = new HashMap<>();
        data.put("reason", reasonStr);

        LogEntry entry = new LogEntry();
        entry.setEventType(type);
        entry.setUserUID(event.getTarget().getClientUniqueID());
        entry.setRawData(serializeMap(data));

        persist(entry);
    }

    private synchronized void persist(LogEntry entry) {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(entry);
            transaction.commit();
        } catch (Exception e) {
            logger.error("Failed to persist log entry: {}", entry.getEventType().getTs3Event(), e);
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    private String serializeMap(Map<String, String> map) {
        StringBuilder builder = new StringBuilder();
        map.forEach((key, value) -> builder.append(key).append(':').append(value).append('\n'));
        return builder.toString();
    }
}
